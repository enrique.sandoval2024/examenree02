/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examenree02;

/**
 *
 * @author enriq
 */
public class PagoQuincenal {
     private int puesto;
   private String nom = null;
   
   private int nivel;
   private int diasTra;
   private int numRec;
   
   
   public PagoQuincenal() {
        this.puesto = 0;
        this.nom = "";
       
        this.nivel = 0;
        this.diasTra = 0;
        this.numRec = 0;
}

    public PagoQuincenal(int puesto, int nivel, int diasTra, int numRec, String nom) {
        this.puesto = puesto;
        this.nivel = nivel;
        this.diasTra = diasTra;
        this.numRec = numRec;
        this.nom = nom;
    }
    public PagoQuincenal(PagoQuincenal otro){
        this.puesto = otro.puesto;
        this.nivel = otro.nivel;
        this.diasTra = otro.diasTra;
        this.numRec = otro.numRec;
        this.nom = otro.nom;
    }
   
   
   

    public int getPuesto() {
        return puesto;
    }

    public void setPuesto(int puesto) {
        this.puesto = puesto;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public int getDiasTra() {
        return diasTra;
    }

    public void setDiasTra(int diasTra) {
        this.diasTra = diasTra;
    }

    public int getNumRec() {
        return numRec;
    }

    public void setNumRec(int numRec) {
        this.numRec = numRec;
    }
   
   
   public float calcularPago(){
       float pago = 0.0f;
       if (this.puesto == 1){
           pago= this.diasTra * 100;
       }
       if (this.puesto == 2){
           pago= this.diasTra * 200;
       }
       if (this.puesto == 3){
           pago= this.diasTra * 300;
       }
       
       return pago;
   }
   
   public float calcularImpuesto(){
       float impuesto = 0.0f;
       if (this.nivel == 1){
           impuesto = this.calcularPago() * 0.05f;
       }
       if (this.nivel == 2){
           impuesto = this.calcularPago() * 0.03f;
       }
       return impuesto;
   }
   public float calcularTotalPagar(){
       float total = 0.0f;
       total = this.calcularPago() - this.calcularImpuesto();
       return total;
   }
   
   
}

